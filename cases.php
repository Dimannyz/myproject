<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Main\Page\Asset as Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/cases-ajax.js');
$APPLICATION->SetTitle("Проекты");
?>
<div class="main-content js-main-content cases-content">
        <section class="page-section cases cases-separate">

		<div class="container">
            <div class="cases__title-box">
              <h1 class="title cases-title">
                <?php 
                  $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                    "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
                    "PATH" => "/include/cases/h1.php",	// Путь к файлу области
                  ),
                  false
                );?>
                </h1>
            </div>
            <div class="cases__inner">
              <div class="cases__filter">
                <form class="filter js-filter" action="/assets/data/works.json" method="GET">
                  <div class="filter__line">
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="type_1"><span class="filter-item__text">Frontend
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="type_2"><span class="filter-item__text">Backend
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="type_3"><span class="filter-item__text">Support
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="type_4"><span class="filter-item__text">Design
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="type_5"><span class="filter-item__text">UX / UI
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="type_6"><span class="filter-item__text">QA
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                  </div>
                  <div class="filter__line">
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="work_1"><span class="filter-item__text">Финансы
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="work_2"><span class="filter-item__text">B2B
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="work_3"><span class="filter-item__text">Телеком
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item"> 
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="work_4"><span class="filter-item__text">Мобильные приложения
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                    <label class="filter-item">
                      <input class="filter-item__input js-filter-item__input" type="checkbox" name="work_5"><span class="filter-item__text">Недвижимость
                        <svg class="icon cross">
                          <use xlink:href="#cross"></use>
                        </svg></span>
                    </label>
                  </div>
                </form>
              </div>
              <?php
                $APPLICATION->IncludeComponent(
                "bitrix:news.list", 
                "projects_list", 
                array(
                  "ACTIVE_DATE_FORMAT" => "d.m.Y",
                  "ADD_SECTIONS_CHAIN" => "Y",
                  "AJAX_MODE" => "N",
                  "AJAX_OPTION_ADDITIONAL" => "",
                  "AJAX_OPTION_HISTORY" => "N",
                  "AJAX_OPTION_JUMP" => "N",
                  "AJAX_OPTION_STYLE" => "N",
                  "CACHE_FILTER" => "N",
                  "CACHE_GROUPS" => "Y",
                  "CACHE_TIME" => "36000000",
                  "CACHE_TYPE" => "A",
                  "CHECK_DATES" => "Y",
                  "DETAIL_URL" => "",
                  "DISPLAY_BOTTOM_PAGER" => "Y",
                  "DISPLAY_DATE" => "Y",
                  "DISPLAY_NAME" => "Y",
                  "DISPLAY_PICTURE" => "Y",
                  "DISPLAY_PREVIEW_TEXT" => "Y",
                  "DISPLAY_TOP_PAGER" => "N",
                  "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                  ),
                  "FILTER_NAME" => "",
                  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                  "IBLOCK_ID" => "6",
                  "IBLOCK_TYPE" => "main",
                  "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                  "INCLUDE_SUBSECTIONS" => "Y",
                  "MESSAGE_404" => "",
                  "NEWS_COUNT" => "10",
                  "PAGER_BASE_LINK_ENABLE" => "N",
                  "PAGER_DESC_NUMBERING" => "N",
                  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                  "PAGER_SHOW_ALL" => "N",
                  "PAGER_SHOW_ALWAYS" => "N",
                  "PAGER_TEMPLATE" => "modern",
                  "PAGER_TITLE" => "Проекты",
                  "PARENT_SECTION" => "",
                  "PARENT_SECTION_CODE" => "",
                  "PREVIEW_TRUNCATE_LEN" => "",
                  "PROPERTY_CODE" => array(
                    0 => "PARTS",
                    1 => "",
                  ),
                  "SET_BROWSER_TITLE" => "N",
                  "SET_LAST_MODIFIED" => "N",
                  "SET_META_DESCRIPTION" => "Y",
                  "SET_META_KEYWORDS" => "Y",
                  "SET_STATUS_404" => "Y",
                  "SET_TITLE" => "N",
                  "SHOW_404" => "N",
                  "SORT_BY1" => "ID",
                  "SORT_BY2" => "SORT",
                  "SORT_ORDER1" => "ASC",
                  "SORT_ORDER2" => "ASC",
                  "STRICT_SECTION_CHECK" => "N",
                  "COMPONENT_TEMPLATE" => "projects_list"
                ),
                false
              );?>
              </div>
            </div>
          </div>
        </section>
        <div class="up-button-section">
          <div class="container">
            <div class="up-button-section__inner"><a class="up-button js-up-button" href="#">
                <div class="up-button__wrap">
                  <div class="up-button__icon-holder"><span class="arrow-up up-button__icon"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
<title>Untitled-1</title>
<g id="Trash" class="arrow-up__head arrow-up__part">
<g id="Logo-Copy-12">
<g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
<g id="Shape">
<path class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"/>
</g>
</g></g>
</g>
<g id="Trash-2" data-name="Trash" class="arrow-up__axis arrow-up__part">
<g id="Logo-Copy-12-2" data-name="Logo-Copy-12"><g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
<g id="Shape-2" data-name="Shape"><path class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"/>
</g>
</g>
</g>
</g>
</svg></span></div><span class="up-button__text">
                    <?php 
                      $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                        "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
                        "PATH" => "/include/cases/buttonUpText.php",	// Путь к файлу области
                      ),
                      false
                    );?>
                  </span>
                </div></a></div>
          </div>
        </div>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>