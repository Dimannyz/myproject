<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if($arResult["OK_MESSAGE"] <> '')
{
	?>
  <div class="contact-form__succes-message js-form-succes">
    <div class="contact-form__succes-inner"><span class="title"><?=$arResult["OK_MESSAGE"]?></span></div>
  </div><?
}
?>
<!-- <form class="contact-form js-contact-form" name="sendmail">
                    <div class="contact-form__row"><span class="title contact-form__title">Свяжитесь с нами</span>
                      <div class="contact-form__close js-close-contact-form"></div>
                    </div> -->
                    <!-- <div class="contact-form__row contact-form__row--name">
                      <input class="contact-form__input contact-form__field js-input js-input-name" type="text" name="name" id="name" required>
                      <div class="contact-form__fill-line"></div>
                      <label class="contact-form__label js-form-label" for="name">Как к вам обращаться?</label>
                    </div>
                    <div class="contact-form__row">
                      <label class="contact-form__label js-form-label" for="phone">Телефон</label>
                      <input class="contact-form__input contact-form__field js-input js-phone customPhone" type="tel" name="phone" id="phone" required>
                      <div class="contact-form__fill-line"></div>
                    </div>
                    <div class="contact-form__row">
                      <label class="contact-form__label js-form-label" for="email">Электропочта</label>
                      <input class="contact-form__input contact-form__field js-input customEmail" type="email" name="email" id="email" required>
                      <div class="contact-form__fill-line"></div>
                    </div>
                    <div class="contact-form__row contact-form__flags">
                      <input class="contact-form__checkbox" type="radio" id="digital" name="form" checked>
                      <label class="contact-form__checkbox-label" for="digital">Digital-разработка</label>
                      <input class="contact-form__checkbox" type="radio" id="up-team" name="form">
                      <label class="contact-form__checkbox-label" for="up-team">Усиление команды</label>
                    </div>
                    <div class="contact-form__row contact-form__row--message">
                      <label class="contact-form__label js-form-label" for="message">Опишите вашу задачу</label>
                      <textarea class="contact-form__input contact-form__field contact-form__input--textarea contact-form__input--textarea_scroll js-input" name="message" id="message"></textarea>
                      <div class="contact-form__fill-line"></div>
                    </div>
                    <div class="contact-form__row contact-form__row--button">
                      <button class="contact-form__input button" type="submit">Отправить</button>
                    </div>
                    <div class="contact-form__row contact-form__row--terms"><span class="contact-form__terms">Нажимая "Отправить" Вы соглашаетесь с&nbsp;</span><a class="link--privacy" href="sogl.pdf" target="_blank">Политикой обработки персональных данных</a></div>
                    <div class="form-error-container"></div>
                    <input class="contact-form__person js-input-person" type="text" name="person">
                  </form> -->




<form class="contact-form js-contact-form" name="sendmail" action="<?=POST_FORM_ACTION_URI?>" method="POST">
<?=bitrix_sessid_post()?>
	<div class="contact-form__row contact-form__row--name">
		<span class="title contact-form__title">Свяжитесь с нами</span>
        <div class="contact-form__close js-close-contact-form"></div>
    </div>
	<div class="contact-form__row">
		<input class="contact-form__input contact-form__field js-input js-input-name" type="text" name="user_name" id="name">
        <div class="contact-form__fill-line"></div>
        <label class="contact-form__label js-form-label" for="user_name"><?=GetMessage("MFT_NAME")?></label>
	</div>
  <div class="contact-form__row">
    <label class="contact-form__label js-form-label" for="user_phone"><?=GetMessage("MFT_PHONE")?></label>
    <input class="contact-form__input contact-form__field js-input js-phone customPhone" type="tel" name="user_phone" id="phone">
    <div class="contact-form__fill-line"></div>
  </div>
  <div class="contact-form__row">
    <label class="contact-form__label js-form-label" for="email"><?=GetMessage("MFT_EMAIL")?></label>
    <input class="contact-form__input contact-form__field js-input customEmail" type="email" name="user_email" id="email">
    <div class="contact-form__fill-line"></div>
  </div>
  <div class="contact-form__row contact-form__flags">
    <input class="contact-form__checkbox" type="radio" id="digital" name="form_flag" checked>
    <label class="contact-form__checkbox-label" for="digital"><?=GetMessage("MFT_DIGITAL")?></label>
    <input class="contact-form__checkbox" type="radio" id="up-team" name="form_flag">
    <label class="contact-form__checkbox-label" for="up-team"><?=GetMessage("MFT_UP_TEAM")?></label>
  </div>
  <div class="contact-form__row contact-form__row--message">
    <label class="contact-form__label js-form-label" for="message"><?=GetMessage("MFT_MESSAGE")?></label>
    <textarea class="contact-form__input contact-form__field contact-form__input--textarea contact-form__input--textarea_scroll js-input" name="message" id="message"></textarea>
    <div class="contact-form__fill-line"></div>
  </div>
	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<div class="mf-captcha">
		<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
		<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="">
	</div>
	<?endif;?>
	<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
  <!-- <div class="contact-form__row contact-form__row--button">
  </div> -->
  
  <div class="contact-form__row contact-form__row--terms"><span class="contact-form__terms">Нажимая "Отправить" Вы соглашаетесь с&nbsp;</span><a class="link--privacy" href="sogl.pdf" target="_blank">Политикой обработки персональных данных</a></div>
  <div class="form-error-container"></div>
  <input class="contact-form__person js-input-person" type="text" name="person">
  <input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
</form>
</div>