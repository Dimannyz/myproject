<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<footer class="footer">
    <div class="container">
        <div class="footer__inner">
            <div class="footer__row">
                <div class="footer__logo"><a class="logo" href="/">
                    <div class="logo__pic">
                    	<?php
							$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
								"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
								"PATH" => "/include/logo.php",	// Путь к файлу области
							),
							false
                		);?>
                    </div></a>
				</div>
            </div>
              	<div class="footer__row">
				  	<?php
					  	$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	                  		"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                    		"AREA_FILE_SUFFIX" => "inc",
                    		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
                    		"PATH" => "/include/privacy.php",	// Путь к файлу области
                  		),
                  		false
                	);?>
				  	<span class="footer__copyright">
						<?php
						  	$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
								"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
								"AREA_FILE_SUFFIX" => "inc",
								"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
								"PATH" => "/include/copyright.php",	// Путь к файлу области
							),
							false
                		);?></span>
				</div>
        </div>
    </div>
</footer>
<div class="cookie js-cookie">
          <div class="container">
            <div class="cookie__inner"><span class="cookie__text">Мы используем файлы cookie, чтобы улучшить работу сайта. Дальнейшее пребывание на сайте означает согласие с их применением. <a href="#">Узнать подробнее</a></span>
              <div class="cookie__button button js-cookie-button">Принять</div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
</div>

	<div class="main-menu-holder">
      <div class="container">
        <div class="main-menu-box">
          <div class="js-nav-menu nav-menu">
            <div class="nav-menu__head">
              <div class="nav-menu__head-inner">
                <div class="mode-icon js-switch-theme nav-menu__mode">
                  <svg class="icon night-mode">
                    <use xlink:href="#night-mode"></use>
                  </svg>
                  <svg class="icon day-mode">
                    <use xlink:href="#day-mode"></use>
                  </svg>
                </div>
              </div>
            </div>
            <div class="nav-menu__wrap">
              <div class="nav-menu__inner js-menu-inner">
                <nav class="main-menu nav-menu__menu menu-top--animate">
                  <ul class="main-menu__list">
                    <li class="main-menu__item"><a class="main-menu__link link--red js-link-services" href="index.html">Главная</a></li>
                    <li class="main-menu__item"><a class="main-menu__link link--red" href="about.html">О компании</a><a class="main-menu__sublink link--red js-menu-anchor" href="about.html#reviews">Отзывы</a><a class="main-menu__sublink link--red js-menu-anchor" href="about.html#publications">Публикации</a><a class="main-menu__sublink link--red js-menu-anchor" href="page-articles.html">Статьи</a></li>
                    <li class="main-menu__item"><a class="main-menu__link link--red js-link-services js-menu-anchor" href="/#services-main">Услуги</a><a class="main-menu__sublink link--red" href="digital.html">Digital-разработка</a><a class="main-menu__sublink link--red" href="up-team.html">Усиление IT-команд</a></li>
                    <li class="main-menu__item"><a class="main-menu__link link--red" href="cases.html">Наши проекты</a></li>
                    <li class="main-menu__item"><a class="main-menu__link link--red" href="career.html">Вакансии</a><a class="main-menu__sublink link--red" href="about.html" target="_blank">Стажировки</a><a class="main-menu__sublink link--red js-menu-anchor" href="career.html#career-culture">Корпоративная культура</a></li>
                    <li class="main-menu__item"><a class="main-menu__link link--red js-link-contacts js-menu-anchor" href="/#contacts">Контакты</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<?php
		$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"projects_popup", 
		array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "N",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"FILTER_NAME" => "",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "6",
			"IBLOCK_TYPE" => "main",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "Y",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "30",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => "arrows",
			"PAGER_TITLE" => "Проекты",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "parts",
				2 => "",
				3 => "",
			),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "Y",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "ID",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_ORDER2" => "ASC",
			"STRICT_SECTION_CHECK" => "N",
			"COMPONENT_TEMPLATE" => "projects_popup"
		),
		false
	);?>
<!-- Yandex.Metrika counter -->
    <!--
    <script async type="text/javascript">
      (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
      ym(26349456, "init", {
           clickmap:true,
           trackLinks:true,
           accurateTrackBounce:true
      });
    </script>
    
    <noscript><div><img src="https://mc.yandex.ru/watch/26349456" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    
    --><!-- /Yandex.Metrika counter -->
  </body>
</html>
